<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/mystyles.css" />
        <title>VUE COMPONENTS</title>
    </head>
    <body>
        <main>
            <section>
                <h1 id="title">{{ title }}</h1>
                <br /><hr />
                <div class="list">
                    <ul>
                        <li>
                            <a href="index.php">Home</a>

                        </li>
                        <li>
                            <a href="v-bind.php">v-bind</a>
                        </li>
                        <li>
                            <a href="class-bind.php">class-binding</a>

                        </li>
                        <li>
                            <a href="components.php">Components</a>

                        </li>
                        <li>
                            <a href="nested-components.php">nested components</a>

                        </li>
                    </ul>

                </div>
            </section>
            <section>

                <div id="demo-1" class="profile text-center">

                    <img v-bind:src="profile.avatar" width="300" />
                    <h3>{{ profile.name }}</h3>
                    <p>
                        {{ profile.bio }}
                    </p>
                    <button @click='changeImg'>Change</button>
                    <button @click='changeBack'>Back</button>
                </div>

            </section>
        </main>



        <script type="text/javascript" src="javascript/vue.js"></script>
        <script type="text/javascript" src="javascript/v-bind.js"></script>

    </body>
</html>
