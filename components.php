<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="css/mystyles.css" />
        <title>VUE COMPONENTS</title>
    </head>
    <body>
        <main>
            <section>
                <h1 id="title">{{ title }}</h1>
                <br /><hr />
                <div class="list">
                    <ul>
                        <li>
                            <a href="index.php">Home</a>

                        </li>
                        <li>
                            <a href="v-bind.php">v-bind</a>
                        </li>
                        <li>
                            <a href="class-bind.php">class-binding</a>

                        </li>
                        <li>
                            <a href="components.php">Components</a>

                        </li>
                        <li>
                            <a href="nested-components.php">nested components</a>

                        </li>
                    </ul>

                </div>
            </section>
            <section id="1">
                <div id="demo-1">
                    <h5>Tasklist</h5>
                    <ul class="list-group">
                        <task v-for="task in tasks">{{ task }}</task>
                    </ul>
                    <task></task>
                </div>
            </section>
            <section id="2">
                <div class="demo-2">
                    <h5>Tasklist</h5>
                    <task-list></task-list>
                </div>
                
            </section>
        </main>



        <script type="text/javascript" src="javascript/vue.js"></script>
        <script type="text/javascript" src="javascript/components.js"></script>
    </body>
</html>
