new Vue({
    el: '#title',
    data: {
        title: 'Vue Components Demo!'
    }
})
var taskList = new Vue({
    el: '#task-div',
    data: {
        task: '',
        taskList: [
            'eat',
            'sleep',
            'repeat'
        ]
    },
    methods: {
        addTask(){

            this.taskList.push(this.task);
        },
        removeTask(){
            this.taskList.pop();
        }
    }
})
