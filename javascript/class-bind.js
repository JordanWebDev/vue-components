new Vue({
    el: '#title',
    data: {
        title: 'Vue Components Demo! - Class Binding'
    }
})
new Vue ({
    el: '.btn',
    data: {
        //green by  default
        buttonGreen: true
    },
    methods: {
        toggleButton(){
            //easiest way to toggle between true and false
            this.buttonGreen = !this.buttonGreen;
        }
    }
})
new Vue ({
    el: '.noborder',
    data: {
        //green by  default
        addBorder: true
    },
    methods: {
        toggleDiv(){
            //easiest way to toggle between true and false
            this.addBorder = !this.addBorder;
        }
    }
})
