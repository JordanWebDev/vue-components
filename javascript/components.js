new Vue({
    el: '#title',
    data: {
        title: 'Vue Components Demo! - Components'
    }
})
//section 1...
Vue.component('task', {
    template: '<div class="task bold"><slot></slot></div>'
})
new Vue({
    el: '#demo-1',
    data: {
        tasks: [
            'make bed',
            'shower',
            'feed cats',
            'nap'
        ]
    }
})
//section 2...
Vue.component('task-list',{
    template: '<ul class="list-group"><task v-for="task in tasks">{{ task }}</task></ul>',
    data(){
        return{
            tasks:[
                'make bed',
                'shower',
                'feed cats',
                'nap'
            ]
        }
    }
})
new Vue({
    el: '#demo-2',
})
