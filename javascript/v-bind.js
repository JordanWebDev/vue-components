new Vue({
    el: '#title',
    data: {
        title: 'Vue Components Demo! - V Bind'
    }
})
new Vue ({
    el: '#demo-1',
    data: {
        profile: {
            name: 'leeroy jenkins',
            avatar: 'img/leeroy-jenkins.jpeg',
            bio: 'leeeeeeeeeeeeeroyyy jenkins'
        }
    },
    methods: {
        changeImg(){

            this.profile.avatar = 'img/leeroy-jenkins2.png';
            this.profile.name = 'Arrrrrgg!!!';
            this.profile.bio = 'Jeeeeeeeeeeeeennnkins';
        },
        changeBack(){
            this.profile.avatar = 'img/leeroy-jenkins.jpeg';
            this.profile.name = 'leeroy jenkins';
            this.profile.bio = 'leeeeeeeeeeeeeroyyy jenkins';
        }
    }
})
